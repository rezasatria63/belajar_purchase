# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
# from odoo.exceptions import UserError, Warning, ValidationError

class Kamar(models.Model):
    """create new model kamar"""
    _name = 'kamar'
    
    # field

    name = fields.Char()
    nomor_kamar = fields.Char()
    kelas_id = fields.Many2one('kelas')
    status_kamar = fields.Selection([('available', 'Available'),
                                        ('booked', 'Booked'),
                                        ('check_in', 'Check In'),
                                        ('check_out', 'Check Out'),
                                        ], string='Status Kamar', required=True, default='available', readonly=True)
    booking_id = fields.Many2one('booking.kamar', compute="compute_booking_kamar")
    
    def compute_booking_kamar(self):
        """get active booked status"""
        for rec in self:
            booking_id = self.env['booking.kamar'].search([('kamar_id','=',rec.id), ('state','!=','check_out')], limit=1)
            rec.booking_id = booking_id
    

    

    
