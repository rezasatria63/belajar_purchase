# -*- coding: utf-8 -*-
"""file purchase order"""
from odoo import models, fields, api, _
from odoo.exceptions import UserError, Warning, ValidationError

class PurchaseOrder(models.Model):
    """inherit models purchase.order"""
    _inherit = 'purchase.order'
    
    # field
    contoh_trigger = fields.Boolean() 
    trigger_warning = fields.Boolean()
    keterangan = fields.Char()
    keterangan_compute = fields.Char(compute="compute_keterangan")
    # many2one
    nomor_hp = fields.Char(related="partner_id.phone")
    
    # fungsi2
    # contoh on change
    @api.onchange('partner_id')
    def on_change_keterangan(self):
        """on change keterangan"""
        self.keterangan = self.partner_id.email + 'abcd'
    
    @api.onchange('trigger_warning')
    def onchange_trigger_warning(self):
        """on change trigger warning"""
        if self.trigger_warning:
            raise Warning(_('warninggg!!!!'))
        
    
    # contoh compute
    @api.depends('partner_id')
    def compute_keterangan(self):
        """compute keterangan"""
        self.keterangan_compute = self.partner_id.email
    
    def show_user_error(self):
        raise UserError('ini user error')
    
    def set_keterangan(self):
        self.keterangan = 'aa bb cc'
    
    # contoh inherit fungsi button cancel
    def button_cancel(self):
        # sebelum fungsi jalan sisipkan code disini
        res = super(PurchaseOrder, self).button_cancel()
        # setelah fungsi jalan sisipkan code disini
        self.set_keterangan()
        return res
    
        
# field ada beberapa attribut -> readonly, invisible, required
        
    
        
        


    

    

    
