# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Purchase - Belajar Purchase',
    'version': '13.0.1.0.0',
    'category': 'Custom',
    'summary': 'Custom module for Purchase.',
    'description': """
            Custom Purchase
    """,
    'website': 'https://www.portcities.net',
    'author':'Reza Satria.',
    'images': [],
    'depends': ['purchase'],
    'data': [
        'views/purchase_order_views.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': False
}
