# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Aplikasi Kuliah',
    'version': '13.0.1.0.0',
    'category': 'Custom',
    'summary': 'Custom module for Sale.',
    'description': """
            Custom
    """,
    'website': 'https://www.portcities.net',
    'author':'Alvin Adji.',
    'images': [],
    'depends': ['base','portal'],
    'data': [
        'security/ir.model.access.csv',
        'data/data.xml',
        'views/jurusan_views.xml',
        'views/mahasiswa_views.xml',
        'views/mata_kuliah_views.xml',
        'views/dosen_views.xml',
        'views/jadwal_views.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': False
}
