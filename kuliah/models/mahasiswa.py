# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
# from odoo.exceptions import UserError, Warning, ValidationError

class Mahasiswa(models.Model):
    """create new model mahasiswa"""
    _name = 'mahasiswa'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    # field
    # track visibility = always , onchange
    name = fields.Char(track_visibility='onchange')
    nik = fields.Char(track_visibility='onchange')
    foto = fields.Binary()
    jadwal_ids = fields.One2many('jadwal', 'mahasiswa_id')
    dosen_id = fields.Many2one('dosen')
    
    

    

    
