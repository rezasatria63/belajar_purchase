# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Sale - Belajar Sale',
    'version': '13.0.1.0.0',
    'category': 'Custom',
    'summary': 'Custom module for Sale.',
    'description': """
            Custom Sale
    """,
    'website': 'https://www.portcities.net',
    'author':'Alvin Adji.',
    'images': [],
    'depends': ['sale'],
    'data': [
        'views/sale_order_views.xml',
        'views/account_move_views.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': False
}
