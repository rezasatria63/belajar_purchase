# -*- coding: utf-8 -*-
"""file purchase order"""
from email.policy import default
import random, string
from odoo import models, fields, api, _
from odoo.exceptions import UserError, Warning, ValidationError

class ProductTemplate(models.Model):
    """inherit models product.template"""
    _inherit = 'product.template'
    
    # field
    
    @api.model
    def create(self, vals):
        if 'default_code' not in vals or ('default_code' in vals and vals['default_code'] == False):
            vals['default_code'] = ''.join(random.choice(string.ascii_uppercase +  string.digits) for _ in range(8))
            check = self.env['product.template'].search([('default_code', '=', vals['default_code'])])
            if check:
                raise Warning('internal reference already exist')
        
        #print("values -> ", vals)
        res = super(ProductTemplate, self).create(vals)
        
        return res

    # ORM
    # self.env['nama.object'].search([]) -> tanpa kondisi
    # self.env['nama.object'].search([('field', 'operator', 'value')]) -> with kondisi
    
    # self/object.nama_fungsi()
    # self/object.nama_field
    

    
