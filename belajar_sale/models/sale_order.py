# -*- coding: utf-8 -*-
"""file purchase order"""

from odoo import models, fields, api, _
from odoo.exceptions import UserError, Warning, ValidationError

class SaleOrder(models.Model):
    """inherit models sale.order"""
    _inherit = 'sale.order'
    
    # field
    keterangan = fields.Char()
    email = fields.Char(related='partner_id.email')
    hide_email = fields.Boolean()
    confirm_status = fields.Boolean(readonly=True, copy=False)
    # many2one
    product_id = fields.Many2one('product.product')
    product_price = fields.Float(related="product_id.lst_price")
    invoice_id = fields.Many2one('account.move')
    dokumen = fields.Binary()
    
    def action_confirm(self):
        res = super(SaleOrder, self).action_confirm()
        self.confirm_status = True
        return res
    
    # fungsi2 CRUD di odoo
    
    # create
    # write -> edit
    # unlink -> delete
    # copy -> duplicate
    
    @api.model
    def create(self, vals):
        res = super(SaleOrder, self).create(vals)
        if not res.hide_email:
            print('test hide')
            raise Warning('hide email must true')
        return res
    
    def write(self, vals):
        res = super(SaleOrder, self).write(vals)
        # if 'hide_email' in vals:
        #     raise Warning('You cant edit hide email')
        return res
    
    def unlink(self):
        if self.hide_email:
            raise Warning('you cant delete this record because this has hide email')
        res = super(SaleOrder, self).unlink()
        return res
    
    @api.returns('self', lambda value: value.id)
    def copy(self, default=None):
        raise UserError(_('You cannot duplicate this SO.'))
        return super(SaleOrder, self).copy(default)
    
    #ORM
    
    # select * from sale_order where id = 5 and hide_email=True -> query 
    # self.env['sale.order'].search([('id', '=', 5), ('hide_email', '=', True)])
    
    
    

    

    

    
